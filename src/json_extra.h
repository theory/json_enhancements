/*-------------------------------------------------------------------------
 *
 * json_extra.h
 *	  Declarations for JSON data type support.
 *
 * Portions Copyright (c) 1996-2012, PostgreSQL Global Development Group
 * Portions Copyright (c) 1994, Regents of the University of California
 *
 * adapted from patch which add to src/include/utils/json.h
 *
 *-------------------------------------------------------------------------
 */

#ifndef JSON_EXTRA_H
#define JSON_EXTRA_H

/* check that we are compiling for the right postgres version */
#if PG_VERSION_NUM >= 90300 ||  PG_VERSION_NUM < 90200
#error Wrong Postgresql version. This software is only for PostgreSQL 9.2
#endif


#include "fmgr.h"
#include "lib/stringinfo.h"

/* functions in json_extra.c */

extern Datum to_json(PG_FUNCTION_ARGS);
extern Datum json_agg_transfn(PG_FUNCTION_ARGS);
extern Datum json_agg_finalfn(PG_FUNCTION_ARGS);

/* functions in jsonfuncs.c */
extern Datum json_object_field(PG_FUNCTION_ARGS);
extern Datum json_object_field_text(PG_FUNCTION_ARGS);
extern Datum json_array_element(PG_FUNCTION_ARGS);
extern Datum json_array_element_text(PG_FUNCTION_ARGS);
extern Datum json_extract_path(PG_FUNCTION_ARGS);
extern Datum json_extract_path_text(PG_FUNCTION_ARGS);
extern Datum json_object_keys(PG_FUNCTION_ARGS);
extern Datum json_array_length(PG_FUNCTION_ARGS);
extern Datum json_each(PG_FUNCTION_ARGS);
extern Datum json_each_text(PG_FUNCTION_ARGS);
extern Datum json_array_elements(PG_FUNCTION_ARGS);
extern Datum json_populate_record(PG_FUNCTION_ARGS);
extern Datum json_populate_recordset(PG_FUNCTION_ARGS);

/* functions in hstore_to_json.c */
extern Datum hstore_to_json(PG_FUNCTION_ARGS);
extern Datum hstore_to_json_loose(PG_FUNCTION_ARGS);
#endif   /* JSON_EXTRA_H */
